/****** Object:  Table [dbo].[AppUsers]    Script Date: 12/03/2020 13:03:44 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[AppUsers](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[UserName] [nvarchar](max) NULL,
	[isAgreeToTerms] [bit] NOT NULL,
 CONSTRAINT [PK_dbo.AppUsers] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Sectors]    Script Date: 12/03/2020 13:03:44 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Sectors](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[Name] [nvarchar](max) NULL,
	[parentId] [int] NOT NULL,
	[depth] [int] NOT NULL,
	[path] [nvarchar](max) NULL,
 CONSTRAINT [PK_dbo.Sectors] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[UserSectors]    Script Date: 12/03/2020 13:03:44 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[UserSectors](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[SectorId] [int] NOT NULL,
	[AppUser_Id] [int] NULL,
 CONSTRAINT [PK_dbo.UserSectors] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET IDENTITY_INSERT [dbo].[AppUsers] ON 

INSERT [dbo].[AppUsers] ([Id], [UserName], [isAgreeToTerms]) VALUES (1, N'a', 1)
INSERT [dbo].[AppUsers] ([Id], [UserName], [isAgreeToTerms]) VALUES (2, N'b', 1)
INSERT [dbo].[AppUsers] ([Id], [UserName], [isAgreeToTerms]) VALUES (3, N'c', 1)
INSERT [dbo].[AppUsers] ([Id], [UserName], [isAgreeToTerms]) VALUES (4, N'd', 1)
INSERT [dbo].[AppUsers] ([Id], [UserName], [isAgreeToTerms]) VALUES (5, N'e', 1)
INSERT [dbo].[AppUsers] ([Id], [UserName], [isAgreeToTerms]) VALUES (6, N'f', 1)
SET IDENTITY_INSERT [dbo].[AppUsers] OFF
SET IDENTITY_INSERT [dbo].[Sectors] ON 

INSERT [dbo].[Sectors] ([Id], [Name], [parentId], [depth], [path]) VALUES (1, N'Manufacturing', 0, 1, N'/1/')
INSERT [dbo].[Sectors] ([Id], [Name], [parentId], [depth], [path]) VALUES (2, N'Construction materials', 1, 2, N'/1/2/')
INSERT [dbo].[Sectors] ([Id], [Name], [parentId], [depth], [path]) VALUES (3, N'Electronics and Optics', 1, 2, N'/1/3/')
INSERT [dbo].[Sectors] ([Id], [Name], [parentId], [depth], [path]) VALUES (4, N'Food and Beverage', 1, 2, N'/1/4/')
INSERT [dbo].[Sectors] ([Id], [Name], [parentId], [depth], [path]) VALUES (5, N'Bakery & confectionery products', 4, 3, N'/1/4/5/')
INSERT [dbo].[Sectors] ([Id], [Name], [parentId], [depth], [path]) VALUES (6, N'Beverages', 4, 3, N'/1/4/6/')
INSERT [dbo].[Sectors] ([Id], [Name], [parentId], [depth], [path]) VALUES (7, N'Fish & fish products ', 4, 3, N'/1/4/7/')
INSERT [dbo].[Sectors] ([Id], [Name], [parentId], [depth], [path]) VALUES (8, N'Meat & meat products', 4, 3, N'/1/4/8/')
INSERT [dbo].[Sectors] ([Id], [Name], [parentId], [depth], [path]) VALUES (9, N'Milk & dairy products ', 4, 3, N'/1/4/9/')
INSERT [dbo].[Sectors] ([Id], [Name], [parentId], [depth], [path]) VALUES (10, N'Other', 4, 3, N'/1/4/10/')
INSERT [dbo].[Sectors] ([Id], [Name], [parentId], [depth], [path]) VALUES (11, N'Sweets & snack food', 4, 3, N'/1/4/11/')
INSERT [dbo].[Sectors] ([Id], [Name], [parentId], [depth], [path]) VALUES (12, N'Furniture', 1, 2, N'/1/12/')
INSERT [dbo].[Sectors] ([Id], [Name], [parentId], [depth], [path]) VALUES (13, N'Bathroom/sauna ', 12, 3, N'/1/12/13/')
INSERT [dbo].[Sectors] ([Id], [Name], [parentId], [depth], [path]) VALUES (14, N'Bedroom', 12, 3, N'/1/12/14/')
INSERT [dbo].[Sectors] ([Id], [Name], [parentId], [depth], [path]) VALUES (15, N'Children’s room ', 12, 3, N'/1/12/15/')
INSERT [dbo].[Sectors] ([Id], [Name], [parentId], [depth], [path]) VALUES (16, N'Kitchen ', 12, 3, N'/1/12/16/')
INSERT [dbo].[Sectors] ([Id], [Name], [parentId], [depth], [path]) VALUES (17, N'Living room ', 12, 3, N'/1/12/17/')
INSERT [dbo].[Sectors] ([Id], [Name], [parentId], [depth], [path]) VALUES (18, N'Office', 12, 3, N'/1/12/18/')
INSERT [dbo].[Sectors] ([Id], [Name], [parentId], [depth], [path]) VALUES (19, N'Other (Furniture)', 12, 3, N'/1/12/19/')
INSERT [dbo].[Sectors] ([Id], [Name], [parentId], [depth], [path]) VALUES (20, N'Outdoor ', 12, 3, N'/1/12/20/')
INSERT [dbo].[Sectors] ([Id], [Name], [parentId], [depth], [path]) VALUES (21, N'Project furniture', 12, 3, N'/1/12/21/')
INSERT [dbo].[Sectors] ([Id], [Name], [parentId], [depth], [path]) VALUES (22, N'Machinery', 1, 2, N'/1/22/')
INSERT [dbo].[Sectors] ([Id], [Name], [parentId], [depth], [path]) VALUES (23, N'Machinery components', 22, 3, N'/1/22/23/')
INSERT [dbo].[Sectors] ([Id], [Name], [parentId], [depth], [path]) VALUES (24, N'Machinery equipment/tools', 22, 3, N'/1/22/24/')
INSERT [dbo].[Sectors] ([Id], [Name], [parentId], [depth], [path]) VALUES (25, N'Manufacture of machinery ', 22, 3, N'/1/22/25/')
INSERT [dbo].[Sectors] ([Id], [Name], [parentId], [depth], [path]) VALUES (26, N'Maritime', 22, 3, N'/1/22/26/')
INSERT [dbo].[Sectors] ([Id], [Name], [parentId], [depth], [path]) VALUES (27, N'Aluminium and steel workboats ', 26, 4, N'/1/22/26/27/')
INSERT [dbo].[Sectors] ([Id], [Name], [parentId], [depth], [path]) VALUES (28, N'Boat/Yacht building', 26, 4, N'/1/22/26/28/')
INSERT [dbo].[Sectors] ([Id], [Name], [parentId], [depth], [path]) VALUES (29, N'Ship repair and conversion', 26, 4, N'/1/22/26/29/')
INSERT [dbo].[Sectors] ([Id], [Name], [parentId], [depth], [path]) VALUES (30, N'Metal structures', 22, 3, N'/1/22/30/')
INSERT [dbo].[Sectors] ([Id], [Name], [parentId], [depth], [path]) VALUES (31, N'Other', 22, 3, N'/1/22/31/')
INSERT [dbo].[Sectors] ([Id], [Name], [parentId], [depth], [path]) VALUES (32, N'Repair and maintenance service', 22, 3, N'/1/22/32/')
INSERT [dbo].[Sectors] ([Id], [Name], [parentId], [depth], [path]) VALUES (33, N'Metalworking', 1, 2, N'/1/33/')
INSERT [dbo].[Sectors] ([Id], [Name], [parentId], [depth], [path]) VALUES (34, N'Construction of metal structures', 33, 3, N'/1/33/34/')
INSERT [dbo].[Sectors] ([Id], [Name], [parentId], [depth], [path]) VALUES (35, N'Houses and buildings', 33, 3, N'/1/33/35/')
INSERT [dbo].[Sectors] ([Id], [Name], [parentId], [depth], [path]) VALUES (36, N'Metal products', 33, 3, N'/1/33/36/')
INSERT [dbo].[Sectors] ([Id], [Name], [parentId], [depth], [path]) VALUES (37, N'Metal works', 33, 3, N'/1/33/37/')
INSERT [dbo].[Sectors] ([Id], [Name], [parentId], [depth], [path]) VALUES (38, N'CNC-machining', 37, 4, N'/1/33/37/38/')
INSERT [dbo].[Sectors] ([Id], [Name], [parentId], [depth], [path]) VALUES (39, N'Forgings, Fasteners ', 37, 4, N'/1/33/37/39/')
INSERT [dbo].[Sectors] ([Id], [Name], [parentId], [depth], [path]) VALUES (40, N'Gas, Plasma, Laser cutting', 37, 4, N'/1/33/37/40/')
INSERT [dbo].[Sectors] ([Id], [Name], [parentId], [depth], [path]) VALUES (41, N'MIG, TIG, Aluminum welding', 37, 4, N'/1/33/37/41/')
INSERT [dbo].[Sectors] ([Id], [Name], [parentId], [depth], [path]) VALUES (42, N'Plastic and Rubber', 1, 2, N'/1/42/')
INSERT [dbo].[Sectors] ([Id], [Name], [parentId], [depth], [path]) VALUES (43, N'Packaging', 42, 3, N'/1/42/43/')
INSERT [dbo].[Sectors] ([Id], [Name], [parentId], [depth], [path]) VALUES (44, N'Plastic goods', 42, 3, N'/1/42/44/')
INSERT [dbo].[Sectors] ([Id], [Name], [parentId], [depth], [path]) VALUES (45, N'Plastic processing technology', 42, 3, N'/1/42/45/')
INSERT [dbo].[Sectors] ([Id], [Name], [parentId], [depth], [path]) VALUES (46, N'Blowing', 45, 4, N'/1/42/45/46/')
INSERT [dbo].[Sectors] ([Id], [Name], [parentId], [depth], [path]) VALUES (47, N'Moulding', 45, 4, N'/1/42/45/47/')
INSERT [dbo].[Sectors] ([Id], [Name], [parentId], [depth], [path]) VALUES (48, N'Plastics welding and processing', 45, 4, N'/1/42/45/48/')
INSERT [dbo].[Sectors] ([Id], [Name], [parentId], [depth], [path]) VALUES (49, N'Plastic profiles', 42, 3, N'/1/42/49/')
INSERT [dbo].[Sectors] ([Id], [Name], [parentId], [depth], [path]) VALUES (50, N'Printing ', 1, 2, N'/1/50/')
INSERT [dbo].[Sectors] ([Id], [Name], [parentId], [depth], [path]) VALUES (51, N'Advertising', 50, 3, N'/1/50/51/')
INSERT [dbo].[Sectors] ([Id], [Name], [parentId], [depth], [path]) VALUES (52, N'Book/Periodicals printing', 50, 3, N'/1/50/52/')
INSERT [dbo].[Sectors] ([Id], [Name], [parentId], [depth], [path]) VALUES (53, N'Labelling and packaging printing', 50, 3, N'/1/50/53/')
INSERT [dbo].[Sectors] ([Id], [Name], [parentId], [depth], [path]) VALUES (54, N'Textile and Clothing', 1, 2, N'/1/54/')
INSERT [dbo].[Sectors] ([Id], [Name], [parentId], [depth], [path]) VALUES (55, N'Clothing', 54, 3, N'/1/54/55/')
INSERT [dbo].[Sectors] ([Id], [Name], [parentId], [depth], [path]) VALUES (56, N'Textile', 54, 3, N'/1/54/56/')
INSERT [dbo].[Sectors] ([Id], [Name], [parentId], [depth], [path]) VALUES (57, N'Wood', 1, 2, N'/1/57/')
INSERT [dbo].[Sectors] ([Id], [Name], [parentId], [depth], [path]) VALUES (58, N'Other (Wood)', 57, 3, N'/1/57/58/')
INSERT [dbo].[Sectors] ([Id], [Name], [parentId], [depth], [path]) VALUES (59, N'Wooden building materials', 57, 3, N'/1/57/59/')
INSERT [dbo].[Sectors] ([Id], [Name], [parentId], [depth], [path]) VALUES (60, N'Wooden houses', 57, 3, N'/1/57/60/')
INSERT [dbo].[Sectors] ([Id], [Name], [parentId], [depth], [path]) VALUES (61, N'Other', 0, 1, N'/61/')
INSERT [dbo].[Sectors] ([Id], [Name], [parentId], [depth], [path]) VALUES (62, N'Creative industries', 61, 2, N'/61/62/')
INSERT [dbo].[Sectors] ([Id], [Name], [parentId], [depth], [path]) VALUES (63, N'Energy technology', 61, 2, N'/61/63/')
INSERT [dbo].[Sectors] ([Id], [Name], [parentId], [depth], [path]) VALUES (64, N'Environment', 61, 2, N'/61/64/')
INSERT [dbo].[Sectors] ([Id], [Name], [parentId], [depth], [path]) VALUES (65, N'Service', 0, 1, N'/65/')
INSERT [dbo].[Sectors] ([Id], [Name], [parentId], [depth], [path]) VALUES (66, N'Business services', 65, 2, N'/65/66/')
INSERT [dbo].[Sectors] ([Id], [Name], [parentId], [depth], [path]) VALUES (67, N'Engineering', 65, 2, N'/65/67/')
INSERT [dbo].[Sectors] ([Id], [Name], [parentId], [depth], [path]) VALUES (68, N'Information Technology and Telecommunications', 65, 2, N'/65/68/')
INSERT [dbo].[Sectors] ([Id], [Name], [parentId], [depth], [path]) VALUES (69, N'Data processing, Web portals, E-marketing', 68, 3, N'/65/68/69/')
INSERT [dbo].[Sectors] ([Id], [Name], [parentId], [depth], [path]) VALUES (70, N'Programming, Consultancy', 68, 3, N'/65/68/70/')
INSERT [dbo].[Sectors] ([Id], [Name], [parentId], [depth], [path]) VALUES (71, N'Software, Hardware', 68, 3, N'/65/68/71/')
INSERT [dbo].[Sectors] ([Id], [Name], [parentId], [depth], [path]) VALUES (72, N'Telecommunications', 68, 3, N'/65/68/72/')
INSERT [dbo].[Sectors] ([Id], [Name], [parentId], [depth], [path]) VALUES (73, N'Tourism', 65, 2, N'/65/73/')
INSERT [dbo].[Sectors] ([Id], [Name], [parentId], [depth], [path]) VALUES (74, N'Translation services', 65, 2, N'/65/74/')
INSERT [dbo].[Sectors] ([Id], [Name], [parentId], [depth], [path]) VALUES (75, N'Transport and Logistics', 65, 2, N'/65/75/')
INSERT [dbo].[Sectors] ([Id], [Name], [parentId], [depth], [path]) VALUES (76, N'Air', 75, 3, N'/65/75/76/')
INSERT [dbo].[Sectors] ([Id], [Name], [parentId], [depth], [path]) VALUES (77, N'Rail', 75, 3, N'/65/75/77/')
INSERT [dbo].[Sectors] ([Id], [Name], [parentId], [depth], [path]) VALUES (78, N'Road', 75, 3, N'/65/75/78/')
INSERT [dbo].[Sectors] ([Id], [Name], [parentId], [depth], [path]) VALUES (79, N'Water', 75, 3, N'/65/75/79/')
SET IDENTITY_INSERT [dbo].[Sectors] OFF
SET IDENTITY_INSERT [dbo].[UserSectors] ON 

INSERT [dbo].[UserSectors] ([Id], [SectorId], [AppUser_Id]) VALUES (3, 13, 3)
INSERT [dbo].[UserSectors] ([Id], [SectorId], [AppUser_Id]) VALUES (4, 14, 4)
INSERT [dbo].[UserSectors] ([Id], [SectorId], [AppUser_Id]) VALUES (6, 1, 1)
INSERT [dbo].[UserSectors] ([Id], [SectorId], [AppUser_Id]) VALUES (8, 16, 2)
INSERT [dbo].[UserSectors] ([Id], [SectorId], [AppUser_Id]) VALUES (9, 14, 2)
INSERT [dbo].[UserSectors] ([Id], [SectorId], [AppUser_Id]) VALUES (10, 14, 5)
INSERT [dbo].[UserSectors] ([Id], [SectorId], [AppUser_Id]) VALUES (11, 16, 6)
INSERT [dbo].[UserSectors] ([Id], [SectorId], [AppUser_Id]) VALUES (12, 16, 1)
SET IDENTITY_INSERT [dbo].[UserSectors] OFF
ALTER TABLE [dbo].[AppUsers] ADD  DEFAULT ((0)) FOR [isAgreeToTerms]
GO
ALTER TABLE [dbo].[UserSectors]  WITH CHECK ADD  CONSTRAINT [FK_dbo.UserSectors_dbo.AppUsers_AppUser_Id] FOREIGN KEY([AppUser_Id])
REFERENCES [dbo].[AppUsers] ([Id])
GO
ALTER TABLE [dbo].[UserSectors] CHECK CONSTRAINT [FK_dbo.UserSectors_dbo.AppUsers_AppUser_Id]
GO

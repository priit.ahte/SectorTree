namespace SectorTree.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class DeleteUserId : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.AppUsers", "isAgreeToTerms", c => c.Boolean(nullable: false));
            DropColumn("dbo.AppUsers", "Email");
            DropColumn("dbo.AppUsers", "FirstName");
            DropColumn("dbo.AppUsers", "LastName");
            DropColumn("dbo.UserSectors", "userId");
        }
        
        public override void Down()
        {
            AddColumn("dbo.UserSectors", "userId", c => c.String());
            AddColumn("dbo.AppUsers", "LastName", c => c.String());
            AddColumn("dbo.AppUsers", "FirstName", c => c.String());
            AddColumn("dbo.AppUsers", "Email", c => c.String());
            DropColumn("dbo.AppUsers", "isAgreeToTerms");
        }
    }
}

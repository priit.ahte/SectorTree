namespace SectorTree.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class InitialCreate : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.AppUsers",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Email = c.String(),
                        FirstName = c.String(),
                        LastName = c.String(),
                        UserName = c.String(),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.UserSectors",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        userId = c.String(),
                        SectorId = c.Int(nullable: false),
                        AppUser_Id = c.Int(),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.AppUsers", t => t.AppUser_Id)
                .ForeignKey("dbo.Sectors", t => t.SectorId, cascadeDelete: true)
                .Index(t => t.SectorId)
                .Index(t => t.AppUser_Id);
            
            CreateTable(
                "dbo.Sectors",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Name = c.String(),
                        parentId = c.Int(nullable: false),
                        depth = c.Int(nullable: false),
                        path = c.String(),
                    })
                .PrimaryKey(t => t.Id);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.UserSectors", "SectorId", "dbo.Sectors");
            DropForeignKey("dbo.UserSectors", "AppUser_Id", "dbo.AppUsers");
            DropIndex("dbo.UserSectors", new[] { "AppUser_Id" });
            DropIndex("dbo.UserSectors", new[] { "SectorId" });
            DropTable("dbo.Sectors");
            DropTable("dbo.UserSectors");
            DropTable("dbo.AppUsers");
        }
    }
}

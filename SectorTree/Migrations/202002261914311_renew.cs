namespace SectorTree.Migrations
{
    using System;
    using System.Data.Entity.Migrations;

    public partial class renew : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.Sectors",
                c => new
                {
                    Id = c.Int(nullable: false, identity: true),
                    Name = c.String(),
                    parentId = c.Int(nullable: false),
                    depth = c.Int(nullable: false),
                    path = c.String(),
                })
                .PrimaryKey(t => t.Id);
                    }

        public override void Down()
        {
        }
    }
}

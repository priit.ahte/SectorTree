﻿using System.IO;
using System.Linq;
using System.Collections.Generic;

namespace SectorTree.Models
{
    // Tree class for reading tree from file and 
    public class Tree
    {
        private TreeNode Root { get; set; }

        // method for tree population from file
        internal void ReadTreeFromFile(string v)
        {
            Root = new TreeNode();
            foreach (var line in File.ReadAllLines(v))
            {
                Root.AddToTree(line);
            }
        }

        // method for tree population from file stream such as POSTed form
        internal void ReadTreeFromStream(Stream s)
        {
            Root = new TreeNode();
            using (var reader = new StreamReader(s))
            {
                while (!reader.EndOfStream)
                    Root.AddToTree(reader.ReadLine());
            }
        }

        // traverses all tree depth first and returns flattened list
        public IEnumerable<TreeNode> Flatten() =>
            Root.Children.SelectMany(c => c.Flatten());
    }


    public class TreeNode
    {
        public string Node { get; set; }
        public TreeNode Parent { get; set; }
        public List<TreeNode> Children { get; set; }

        public TreeNode()
        {
            Children = new List<TreeNode>();
        }

        // calculates depth of current node. default depth is 0
        public int getDepth()
        {
            int depth = 0;
            var current = this;
            while (current.Parent != null)
            {
                depth++;
                current = current.Parent;
            }
            return depth;
        }

        // generates path of node values from parent to current Node
        public string getPath() =>
            Parent == null
                ? $"/{Node}/"
                : Parent.getPath() + $"{Node}/";

        // adds sectors to tree from indented file
        public void AddToTree(string line, string depthString = "    ")
        {
            if ( line.StartsWith(depthString) )
            {
                Children.Last().AddToTree(line.Remove(0, depthString.Length), depthString);
            }
            else 
            { 
                Children.Add(new TreeNode { Node = line, Parent = this });
            }
        }

        // traverses TreeNode and flattens to list
        public IEnumerable<TreeNode> Flatten() => 
            new[] { this }.Concat( Children.SelectMany(c => c.Flatten()) );
        
    }

}
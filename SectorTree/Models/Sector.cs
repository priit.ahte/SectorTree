using System.Data.Entity;
using System.Collections.Generic;

namespace SectorTree.Models
{

    public class SectorContext : DbContext
    {
        // Your context has been configured to use a 'Sector' connection string from your application's 
        // configuration file (App.config or Web.config). By default, this connection string targets the 
        // 'SectorTree.Models.Sector' database on your LocalDb instance. 
        // 
        // If you wish to target a different database and/or database provider, modify the 'Sector' 
        // connection string in the application configuration file.
        public SectorContext()
            : base("name=Sector") { }

        // Add a DbSet for each entity type that you want to include in your model. For more information 
        // on configuring and using a Code First model, see http://go.microsoft.com/fwlink/?LinkId=390109.
        public virtual DbSet<Sector> Sectors { get; set; }
        public virtual DbSet<UserSector> UserSectors { get; set; }
        public virtual DbSet<AppUser> AppUsers { get; set; }
    }

    public class Sector
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public int parentId { get; set; }
        public int depth { get; set; }
        public string path { get; set; }
    }

    public class UserSector
    {
        public int Id { get; set; }
        public int SectorId { get; set; }
        public virtual AppUser AppUser { get; set; }
        public virtual Sector Sector { get; set; }
    }

    public class AppUser
    {
        public int Id { get; set; }
        public string UserName { get; set; }
        public bool isAgreeToTerms { get; set; }
        public virtual ICollection<UserSector> UserSectors { get; set; }
    }
}
﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace SectorTree.Models
{
    public class SectorTreeViewModel : IValidatableObject
    {
        [Required(ErrorMessage = "Choose at least one Sector")]
        [Display(Name = "Sectors")]
        public int[] SelectedSectors { get; set; }

        [Required(ErrorMessage = "Enter User Name")]
        [Display(Name = "Name")]
        public string UserName { get; set; }

        [Range(typeof(bool), "true", "true", ErrorMessage = "Please agree to terms")]
        [Display(Name = "Agree to Terms")]
        public bool isAgreeToTerms { get; set; }
        public MultiSelectList Sectors { get; set; }
        public int AppUserId { get; set; }

        private static int TREE_INDENTATION = 4;

        public SectorTreeViewModel()
        {
            using (var db = new SectorContext())
            {
                var sectorList = db.Sectors.OrderBy(x => x.path).ToList().Select(x => new
                {
                    sectorId = x.Id,
                    sectorName = new string('\xA0', (x.depth - 1) * TREE_INDENTATION) + x.Name,
                    depth = x.depth
                });

                Sectors = new MultiSelectList(sectorList, "SectorId", "SectorName");
            }

        }

        public SectorTreeViewModel(int Id) : this()
        {
            AppUserId = Id;
            using (var db = new SectorContext())
            {
                try
                {
                    var AppUser = db.AppUsers.Find(AppUserId);
                    UserName = AppUser.UserName;
                    isAgreeToTerms = AppUser.isAgreeToTerms;
                    SelectedSectors = AppUser.UserSectors.Select(x => x.SectorId).ToArray();
                }
                catch
                {
                    AppUserId = 0;
                    throw new HttpException(404, "Resource not found!");
                }
            }
        }


        public IEnumerable<ValidationResult> Validate(ValidationContext validationContext)
        {
            var db = new SectorContext();
            var AppUser = db.AppUsers.Where(x => x.UserName == UserName).SingleOrDefault();

            // check if user with current name already exists.
            // If so chekc if Id's match. Otherwise add already saved data from database and add model validation error. 
            // This is needed in order to prevent overwriting already existing user data
            if (AppUser != null)
            {
                if (AppUser.Id != AppUserId)
                {
                    AppUserId = AppUser.Id;

                    SelectedSectors = SelectedSectors.Concat(
                        AppUser.UserSectors
                            .Select(x => x.SectorId
                            )).ToArray();

                    yield return new ValidationResult(
                        @"user with this name already exists in database! 
                            Added saved choices to your selection. 
                            You can reselect below and hit Save again to overwrite 
                            current choices.");
                }
            }
        }
    }
}

﻿using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using SectorTree.Models;

namespace SectorTree.Controllers
{
    public class HomeController : Controller
    {
        public ActionResult Index(int AppUserId = 0)
        {
            return AppUserId == 0
                    ? View(new SectorTreeViewModel())
                    : View(new SectorTreeViewModel(AppUserId));
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Save([Bind(Include = "AppUserId,UserName,SelectedSectors,isAgreeToTerms")] SectorTreeViewModel Model)
        {

            AppUser AppUser;
            // User already exists validation error. Update ModelState with data from updated Model
            // See Validate method in SectorTreeViewModel class
            if (!ModelState.IsValid && ModelState.Keys.Contains(""))
            {
                ModelState.SetModelValue("SelectedSectors",
                    new ValueProviderResult(Model.SelectedSectors, "",
                        null));

                ModelState.SetModelValue("AppUserId",
                    new ValueProviderResult(Model.AppUserId, "",
                        null));
            }

            if (ModelState.IsValid)
            {
                using (var db = new SectorContext())
                {
                    AppUser = db.AppUsers.Where(x => x.UserName == Model.UserName).SingleOrDefault();
                    // Add new user data to database
                    if (AppUser != null)
                    {
                        // Remove deselected sectors from Database
                        foreach (var x in AppUser.UserSectors.ToList())
                        {
                            if (!Model.SelectedSectors.Contains(x.SectorId))
                            {
                                db.UserSectors.Remove(x);
                                db.SaveChanges();
                            }
                        }

                        // Add new choice only if sector is not already in database 
                        foreach (var sectorId in Model.SelectedSectors)
                        {
                            if (!AppUser.UserSectors.Any(x => x.SectorId == sectorId))
                            {
                                AppUser.UserSectors.Add(new UserSector { SectorId = sectorId });
                                db.SaveChanges();
                            }
                        }
                    }
                    // Add existing user data to database
                    else
                    {

                        AppUser = db.AppUsers.Add(new AppUser { UserName = Model.UserName, isAgreeToTerms = Model.isAgreeToTerms });
                        db.SaveChanges();

                        foreach (var sectorId in Model.SelectedSectors)
                            db.UserSectors.Add(
                                new UserSector
                                {
                                    SectorId = sectorId,
                                    AppUser = AppUser
                                }); db.SaveChanges();

                    }

                }
                //RedirectToAction("Index");
                return RedirectToAction("Index", new { AppUserId = AppUser.Id });
            }

            return View("Index", Model);
        }

        [HttpPost]
        public ActionResult UploadFile(HttpPostedFileBase file)
        {
            try
            {
                if (file.ContentType.Equals("text/plain") && file.ContentLength > 0)
                {
                    // create new Tree structure by reading data from user posted file stream
                    // file stream has to be properly indented
                    var sectorTree = new Tree();
                    sectorTree.ReadTreeFromStream(file.InputStream);

                    // flatten tree and write it to database
                    using (var db = new SectorContext())
                    {
                        Stack<Sector> parentStack = new Stack<Sector>();
                        Sector parentEntry = null;
                        Sector lastEntry = null;

                        foreach (var current in sectorTree.Flatten())
                        {
                            // if current depth is creater than last entry, 
                            // then parent is pushed to stack and previous entry becomes parent
                            if (current.getDepth() > (lastEntry?.depth ?? 0))
                            {
                                parentStack.Push(parentEntry);
                                parentEntry = lastEntry;
                            }
                            else if (current.getDepth() < (lastEntry?.depth ?? 0))
                            {
                                // take the parent from stack of parents until found parent for current entry
                                do
                                {
                                    parentEntry = parentStack.Pop();
                                } while ((parentEntry?.depth ?? 0) >= current.getDepth());
                            }


                            // save sector to database
                            lastEntry = db.Sectors.Add(
                                    new Sector
                                    {
                                        Name = current.Node,
                                        depth = current.getDepth(),
                                        parentId = parentEntry?.Id ?? 0
                                    });
                            db.SaveChanges();
                        }

                        // generate path column recursively
                        foreach (var row in db.Sectors)
                            row.path = genPath(db, row);
                        db.SaveChanges();
                    }
                }
                return RedirectToAction("Index");
            }
            catch
            {
                throw new HttpException(404, "File upload failed!!");
            }
        }

        // method for path string generation. Recursively finds parentId of current record 
        // from database and adds current record Id to it separated by /
        // Ex. output from parent to child: /1/2/4/5/
        public string genPath(SectorContext db, Sector current)
        {
            return current.parentId != 0
                ? genPath(db, db.Sectors.Find(current.parentId)) + $"{ current.Id }/"
                : $"/{current.Id}/";
        }
    }
}
Kirjutatud kasutades .NET Framework MVC 5

* Puu struktuuri lugemine failist objektiks (categories.txt)
* Puu struktuuri objekti tasandamine ja kirjutamine andmebaasi (meetod Materialized path)
* võimalik lisada ja muuta andmebaasis kasutajate valdkondade kohta andmeid
* Kliendipoolne vormi valideerimine (HTML 5 Constraint APi + Bootstrap 4)
* Serveripoolne vormi valideerimine

scripts.sql
Andmebaasi genereerimisskript ja INSERT laused (T-SQL)

index.html
HTML fail mille programm genereerib